// userController.js

const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const mongoose = require("mongoose");



module.exports.checkEmailExists = (reqBody) => {

  return User.find({ email : reqBody.email }).then(result => {

    if(result.length > 0){

      return true;
    } else {

      return false;
    }
  }).catch(err => {

        console.log(err);

        return false;
    });
};


// User Registration
module.exports.registerUser = (reqBody) => {
  // Create a new User object with the provided user details
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10) // Encrypt the password using bcrypt
  });

  // Save the created object to our database
  return newUser.save()
    .then(user => true)
    .catch(err => {
      console.log(err);
      return false;
    });
};

// User authentication (login)
module.exports.loginUser = reqBody => {
  return User.findOne({ email: reqBody.email })
  .then(result => {
    if (result == null) {
      console.log("user not registered")
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        console.log("password not registered")
        return false;
      }
    }
  });
};

// Function to generate a random unique ID for orderID
const generateUniqueID = () => {
  const timestamp = Date.now().toString(36);
  const randomChars = Math.random().toString(36).substr(2, 5);
  return timestamp + randomChars;
};

// checkout function
module.exports.order = async (data) => {
  // Check if userId is provided
  if (!data.userId) {
    return { success: false, message: "userId is required" };
  }

  // Check if productId is provided
  if (!data.productId) {
    return { success: false, message: "productId is required" };
  }

  try {
    // Find the user with the provided userId
    const user = await User.findById(data.userId);
    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Check if the user is an admin
    if (user.role === 'admin') {
      return { success: false, message: "Admin users are not allowed to checkout" };
    }

    // Find the product with the provided productId
    const product = await Product.findById(data.productId);
    if (!product) {
      return { success: false, message: "Product not found" };
    }

    // Check if the product is still active
    if (!product.isActive) {
      return { success: false, message: "Product is no longer available" };
    }

    // Check if the user has already ordered the same product
    const existingOrder = user.orderedProducts.find(
      (orderedProduct) => orderedProduct.product.toString() === product._id.toString()
    );

    if (existingOrder) {
      // Increment the quantity and update the totalAmount
      existingOrder.quantity += 1;
      existingOrder.totalAmount += product.price;
    } else {
      // Create a new orderedProduct object
      const orderedProduct = {
        product: product._id,
        productName: product.name,
        quantity: 1,
        totalAmount: product.price,
      };

      user.orderedProducts.push(orderedProduct);
    }

    // Update the user reference and generate a unique order ID
    let orderID = generateUniqueID();
    while (product.userOrders.some((order) => order.orderID === orderID)) {
      orderID = generateUniqueID();
    }

    product.userOrders.push({
      user: user._id,
      orderID: orderID
    });

    // Save the updated user and product data
    await user.save();
    await product.save();

    return { success: true };
  } catch (err) {
    console.log(err);
    return { success: false, message: "Error on creating order" };
  }
};

// Function to get user details
module.exports.getUserDetails = async (userId) => {
  try {
    // Find the user in the database by userId
    const user = await User.findById(userId);
    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Reassign the password of the returned document to an empty string
    user.password = '';

    return { success: true, user };
  } catch (err) {
    console.log(err);
    return { success: false, message: "Error retrieving user details" };
  }
};


// Function to set a user as an admin
module.exports.setUserAsAdmin = async (userId) => {
  try {
    // Find the user with the provided userId
    const user = await User.findById(userId);
    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Set the user as an admin
    user.isAdmin = true;
    await user.save();
    return { success: true };
  } catch (err) {
    console.log(err);
    return { success: false, message: "Error setting user as admin" };
  }
};

// Function to get user orders
module.exports.getUserOrders = async (userId) => {
  try {
    // Find the user with the provided userId and populate the orderedProducts with product details
    const user = await User.findById(userId).lean().populate({
      path: 'orderedProducts.product',
      select: '-_id name price',
    });

    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Format the orders data
    const orders = user.orderedProducts.map((orderedProduct) => ({
      orderID: orderedProduct._id,
      product: orderedProduct.product,
    }));

    return { success: true, orders };
  } catch (err) {
    console.log(err);
    return { success: false, message: "Error retrieving user orders" };
  }
};

// Function to get all orders
module.exports.getAllOrders = async () => {
  try {
    const orders = await User.find({}, { orderedProducts: 1 })
      .populate("orderedProducts.product", "-_id name price")
      .lean();

    return { success: true, orders };
  } catch (err) {
    console.log(err);
    return { success: false, message: "Error retrieving all orders" };
  }
};



