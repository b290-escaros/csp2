// productController.js
const Product = require("../models/Product");
const auth = require("../auth");

// Creating a product
module.exports.createProduct = reqBody => {
  // Create a new Product instance with the provided data
  let newProduct = new Product({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
    // Add other properties as needed
  });

  // Save the created product to the database
  return newProduct.save()
    .then(products => true)
    .catch(err => {
      console.log(err);
      return false;
    });
};

// Get all products (requires admin authentication)
module.exports.getAllProducts = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (!userData.isAdmin) {
    return res.status(403).json(false);
  }

  Product.find()
    .then(products => res.send(products))
    .catch(err => {
      console.log(err);
      res.status(500).send("Error retrieving products");
    });
};

// Get all active products
module.exports.getAllActiveProducts = (req, res) => {
  // Find all products that are active
  Product.find({ isActive: true })
    .then(products => res.json(products))
    .catch(error => {
      console.log(error);
      res.status(500).json({ error: "Internal Server Error" });
    });
};

// Get a product by its ID
module.exports.getProductById = (req, res) => {
  const productId = req.params.productId;
  
  // Find a product by its ID
  Product.findById(productId)
    .then(product => {
      if (!product) {
        return res.status(404).json({ error: "Product not found" });
      }
      res.json(product);
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({ error: "Internal Server Error" });
    });
};

// Update a product (requires admin authentication)
module.exports.updateProduct = (req, res) => {
  const productId = req.params.productId;
  const updatedProduct = req.body;
  const userData = auth.decode(req.headers.authorization);

  if (!userData.isAdmin) {
    return res.status(403).json(false);
  }

  // Find and update a product by its ID
  Product.findByIdAndUpdate(productId, updatedProduct, { new: true })
    .then(product => {
      if (!product) {
        return res.status(404).json({ error: "Product not found" });
      }
      res.json(product);
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({ error: "Internal Server Error" });
    });
};

// Archive a product (requires admin authentication)
module.exports.archiveProduct = (req, res) => {
  const productId = req.params.productId;
  const userData = auth.decode(req.headers.authorization);

  if (!userData.isAdmin) {
    return res.status(403).json(false);
  }

  // Find and update a product by its ID to set isActive to false (archived)
  Product.findByIdAndUpdate(productId, { isActive: false }, { new: true })
    .then(product => {
      if (!product) {
        return res.status(404).json({ error: "Product not found" });
      }
      res.json({ success: "Product successfully archived" });
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({ error: "Internal Server Error" });
    });
};

// Activate a product (requires admin authentication)
module.exports.activateProduct = (req, res) => {
  const productId = req.params.productId;
  const userData = auth.decode(req.headers.authorization);

  if (!userData.isAdmin) {
    return res.status(403).json(false);
  }

  // Find and update a product by its ID to set isActive to true (activated)
  Product.findByIdAndUpdate(productId, { isActive: true }, { new: false })
    .then(product => {
      if (!product) {
        return res.status(404).json({ error: "Product not found" });
      }
      res.json({ success: "Product successfully activated" });
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({ error: "Internal Server Error" });
    });
};
