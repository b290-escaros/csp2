// auth.js
const jwt = require("jsonwebtoken");

const secret = "E-Commerce";

// Token creation
module.exports.createAccessToken = user => {
  // Create a token payload with user data
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  };
  
  // Generate a JWT (JSON Web Token) using the payload and secret key
  return jwt.sign(data, secret, {});
};

// Token verification
module.exports.verify = (req, res, next) => {
  const token = req.headers.authorization;

  // Check if the token exists in the request headers
  if (typeof token === "undefined") {
    return res.status(401).send({ success: false, message: "Unauthorized" });
  }

  // Extract the token without the "Bearer " prefix
  const tokenWithoutBearer = token.slice(7);
  
  // Verify the token using the secret key
  jwt.verify(tokenWithoutBearer, secret, (err, data) => {
    if (err) {
      return res.status(401).send({ success: false, message: "Unauthorized" });
    } else {
      req.user = data; // Set the decoded token data on the request object
      next(); // Proceed to the next middleware
    }
  });
};


// Token decoding
module.exports.decode = token => {
  // Check if the token exists
  if (typeof token !== "undefined") {
    // Slice the token to remove the "Bearer " prefix
    token = token.slice(7, token.length);
    
    // Verify the token and extract the payload
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null; // Return null if the token is invalid
      } else {
        // Decode the token and return the payload
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  } else {
    return null; // Return null if the token is missing
  }
};
