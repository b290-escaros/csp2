// productRoute.js
const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product (requires authentication and isAdmin)
router.post("/", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  // Check if the user is an admin
  if (userData.isAdmin) {
    productController.createProduct(req.body)
      .then(resultFromController => res.send(resultFromController));
  } else {
    console.log({ auth: "unauthorized user" });
    res.send(false);
  }
});

// Route for retrieving all products (requires authentication and isAdmin)
router.get("/", auth.verify, productController.getAllProducts);

// Route for retrieving all active products
router.get("/active", productController.getAllActiveProducts);

// Route for retrieving a single product by ID
router.get("/:productId", productController.getProductById);

// Route for updating a product (requires authentication and isAdmin)
router.put("/:productId", auth.verify, productController.updateProduct);

// Route for archiving a product (requires authentication and isAdmin)
router.put("/archive/:productId", auth.verify, productController.archiveProduct);

// Route for activating a product (requires authentication and isAdmin)
router.put("/activate/:productId", auth.verify, productController.activateProduct);

module.exports = router;
