// userRoute.js
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");
const User = require('../models/User');




router.post("/checkEmail", (req, res) => {
  userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
  // Route for user registration
  userController.registerUser(req.body)
    .then(resultFromController => res.send(resultFromController));
});

// User Authentication
router.post("/login", (req, res) => {
  // Route for user login/authentication
  userController.loginUser(req.body)
    .then(resultFromController => res.send(resultFromController));
});

// Route to get user details (requires authentication)
router.get("/details", auth.verify, (req, res) => {
  const userId = req.user.id;
  
  // Check if the requesting user is an admin
  if (req.user.isAdmin) {
    userController.getUserDetails(userId)
      .then(resultFromController => res.send(resultFromController));
  } else {
    res.status(403).send({ success: false, message: "Access denied. Only admins can retrieve user details" });
  }
});


// Route to create a checkout for a non-admin user (requires authentication)
router.post("/checkout", auth.verify, (req, res) => {
  // Extract user data from the decoded token
  const userData = auth.decode(req.headers.authorization);
  
  // Check if the user is an admin
  if (userData.isAdmin) {
    return res.status(403).send({ success: false, message: "Admin users are not allowed to checkout" });
  }

  // Prepare data for the order
  const data = {
    userId: userData.id,
    productId: req.body.productId
  };

  if (!data.productId) {
    return res.status(400).send({ success: false, message: "productId is required" });
  }

  userController.order(data)
    .then(resultFromController => {
      if (!resultFromController.success) {
        return res.send(resultFromController);
      }

      // Order created successfully
      res.send({ success: true, message: "Order created successfully" });
    })
    .catch(err => {
      console.log(err);
      res.status(500).send({ success: false, message: "Internal server error" });
    });
});



// Route to get user details (requires authentication)
router.get("/userDetails/:userId", auth.verify, (req, res) => {
  const userId = req.params.userId;

  userController.getUserDetails(userId)
    .then(resultFromController => res.send(resultFromController));
});



// Route to set a user as an admin (requires authentication and admin privileges)
router.put("/admin/:userId", auth.verify, (req, res) => {
  const userId = req.params.userId;

  // Check if the requesting user is an admin
  if (!req.user.isAdmin) {
    return res.status(403).send({ success: false, message: "Access denied. Only admins can set user as admin" });
  }

  userController.setUserAsAdmin(userId)
    .then(resultFromController => res.send(resultFromController));
});

// Route to get user orders (requires authentication)
router.get("/orders/:userId", auth.verify, (req, res) => {
  const userId = req.params.userId;

  // Check if the requesting user is the same as the authenticated user
  if (userId !== req.user.id) {
    return res.status(403).send({ success: false, message: "Access denied. You can only retrieve your own orders" });
  }

  userController.getUserOrders(userId)
    .then(resultFromController => res.send(resultFromController));
});

router.get("/orders", auth.verify, (req, res) => {
  // Check if the requesting user is an admin
  if (!req.user.isAdmin) {
    return res.status(403).send({ success: false, message: "Access denied. Only admins can retrieve all orders" });
  }

  userController.getAllOrders()
    .then(resultFromController => res.send(resultFromController));
});

module.exports = router;
